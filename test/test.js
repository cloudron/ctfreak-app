#!/usr/bin/env node

'use strict';

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';
    const PROJECT_NAME = 'test project cloudron ' + Math.floor((Math.random() * 100) + 1);
    const TASK_NAME = 'test task cloudron' + Math.floor((Math.random() * 100) + 1);
    const TASK_URL = 'cloudron.io';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    let browser, app, cloudronName;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });


    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');

        const tmp = execSync(`cloudron exec --app ${app.id} env`).toString().split('\n').find((l) => l.indexOf('CLOUDRON_OIDC_PROVIDER_NAME=') === 0);
        if (tmp) cloudronName = tmp.slice('CLOUDRON_OIDC_PROVIDER_NAME='.length);
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function loginOIDC(username, password, alreadyAuthenticated = false) {
        await browser.get('https://' + app.fqdn + '/#/login/');

        await waitForElement(By.xpath(`//button[contains(., "${cloudronName}")]`));
        await browser.findElement(By.xpath(`//button[contains(., "${cloudronName}")]`)).click();

        await browser.sleep(2000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await waitForElement(By.xpath('//h1[@slot="title" and contains(text(), "Projects")]'));
    }

    async function login(username, password) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/#/login/');
        await browser.navigate().refresh(); // hard refresh needed after upgrades // await browser.executeScript('location.reload(true);');
        await waitForElement(By.id('loginUsername'));
        await browser.findElement(By.id('loginUsername')).sendKeys(username);
        await browser.findElement(By.id('loginPassword')).sendKeys(password);
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//button[text()="Log in"]')).click();
        await browser.sleep(3000);
        await waitForElement(By.xpath('//h1[@slot="title" and contains(text(), "Projects")]'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/#/login/logOut');
        await browser.sleep(3000);
        await browser.manage().deleteAllCookies();
        await waitForElement(By.xpath('//button[text()="Log in"]'));
    }

    async function contentExists() {
        await browser.get('https://' + app.fqdn + '/#/projects');
        await waitForElement(By.xpath(`//a[text()="${PROJECT_NAME}"]`));
        await browser.findElement(By.xpath(`//a[text()="${PROJECT_NAME}"]`)).click();
        await browser.wait(until.elementLocated(By.xpath(`//a[text()="${TASK_NAME}"]`)), 10000);
    }

    async function createProject() {
        await browser.get('https://' + app.fqdn + '/#/projects');
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[contains(., "New project")]')).click();
        await browser.sleep(2000);

        await browser.findElement(By.id('newProjectProjectName')).sendKeys(`${PROJECT_NAME}`);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[text()="Create"]')).click();
        await browser.sleep(3000);
        await waitForElement(By.xpath(`//h1[@slot="title" and text()="${PROJECT_NAME}"]`));
        await browser.sleep(2000);
    }

    async function addTask() {
        await browser.get('https://' + app.fqdn + '/#/projects');
        await browser.sleep(2000);
        await waitForElement(By.xpath(`//a[text()="${PROJECT_NAME}"]`));
        await browser.findElement(By.xpath(`//a[text()="${PROJECT_NAME}"]`)).click();
        await browser.sleep(3000);

        await browser.findElement(By.xpath('//button[contains(.,"New task")]')).click();
        await browser.sleep(3000);

        await browser.findElement(By.xpath('//button[contains(.,"HTTP Request")]')).click();
        await browser.sleep(3000);

        await browser.findElement(By.id('taskEditName')).sendKeys(TASK_NAME);
        await browser.findElement(By.id('taskEditHttpRequestTaskUrl')).sendKeys(TASK_URL);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[text()="Create"]')).click();
        await browser.sleep(2000);
        await waitForElement(By.xpath(`//h1[@slot="title" and text()="${TASK_NAME}"]`));
        await browser.sleep(2000);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // no SSO
    it('install app (no sso)', function () { execSync(`cloudron install --no-sso --appstore-id com.ctfreak.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, 'admin', 'ctfreak'));
    it('can logout', logout);
    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // SSO
    it('install app (sso)', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, 'admin', 'ctfreak'));
    it('can create project', createProject);
    it('can add task', addTask);
    it('check project and task exist', contentExists);
    it('can logout', logout);

    it('can OIDC login', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD, false));
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can admin login', login.bind(null, 'admin', 'ctfreak'));
    it('check project and task exist', contentExists);
    it('can logout', logout);

    it('can OIDC login', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD, true));
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw --app ' + app.id));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can admin login', login.bind(null, 'admin', 'ctfreak'));
    it('check project and task exist', contentExists);
    it('can logout', logout);

    it('can OIDC login', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD, true));
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, 'admin', 'ctfreak'));
    it('check project and task exist', contentExists);
    it('can logout', logout);

    it('can OIDC login', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD, true));
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('install app for update', function () {
        execSync(`cloudron install --appstore-id com.ctfreak.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, 'admin', 'ctfreak'));
    it('can create project', createProject);
    it('can add task', addTask);
    it('check project and task exist', contentExists);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, 'admin', 'ctfreak'));
    it('check project and task exist', contentExists);
    it('can logout', logout);

    it('can OIDC login', loginOIDC.bind(null, process.env.USERNAME, process.env.PASSWORD, true));
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
