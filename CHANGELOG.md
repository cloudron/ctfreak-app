[0.1.0]
* Initial version with Ctfreak 1.9.2

[0.2.0]
* Update appstore metadata

[0.3.0]
* Update tags

[0.4.0]
* Enable OIDC support (requires Cloudron 7.5.0)

[0.5.0]
* Update Ctfreak to 1.10.0
* [Full changelog](https://ctfreak.com/docs/changelog#v1-10-0)

[0.6.0]
* Update Ctfreak to 1.10.1
* [Full changelog](https://ctfreak.com/docs/changelog#v1-10-1)
* Add command line arguments to setup external url and main OpenID Connect auth provider at startup of Ctfreak
* Call OpenID Connect userinfo endpoint as fallback for missing claims in ID Token

[1.0.0]
* Initial stable release
* Add optional sso

[1.1.0]
* Update Ctfreak to 1.11.0
* [Full changelog](https://ctfreak.com/docs/changelog#v1-11-0)
* New SQL Report task:
* New project viewer role:
* Redesign of sub-execution database storage
* New task type selector
* Update dependencies
* The FREE Edition now allows creating up to 15 tasks

[1.2.0]
* Update Ctfreak to 1.12.0
* [Full changelog](https://ctfreak.com/docs/changelog#v1-12-0)
* In SQL Report tasks: Charts can be duplicated, New options to force axes to begin at zero
* Improve UI: Increase selection area size for links, Reduces footer refresh rate, Update colors
* Add license alerting: Remaining days before license expiration, License check issues, New About page with third-party software list

[1.3.0]
* Update base image to 4.2.0

[1.4.0]
* Update ctfreak to 1.13.0
* [Full changelog](https://ctfreak.com/docs/changelog#v1-13-0)
* Project constants: Define a list of constants per project, Use them in your bash or powershell script tasks as environment variables, Update the value of your constants instead of updating your scripts
* Replace deprecated dependency to manage OpenID Connect token
* Migrate front-end to Svelte 4
* Update dependencies
* Reworking of demo project

[1.4.1]
* Update ctfreak to 1.13.1
* [Full changelog](https://ctfreak.com/docs/changelog#v1-13-1)
* Migrate to CodeMirror 6
* Update dependencies
* Reworking of license page
* Fix line wrap overflow issue when editing scripts
* Fix chart duplication issue on SQL report tasks

[1.4.2]
* Update ctfreak to 1.33.2
* [Full changelog](https://ctfreak.com/docs/changelog#v1-13-2)
* Support LOGIN SMTP authentication method in addition to PLAIN to enable email notifications to be sent to MS Office 365 smtp server

[1.5.0]
* Update ctfreak to 1.14.0
* [Full changelog](https://ctfreak.com/docs/changelog#v1-14-0)
* !!Breaking change!!: the : symbols in SQL report task queries must now be doubled, so as not to be confused with a query parameter
* Parameterizable tasks: 
* Project constants can now be used in your SQL report tasks as query parameters
* `CF_PC_` project constant prefixes have been renamed to CPC_ (renaming in existing bash and powershell scripts will be done automatically on Ctfreak instance update)
* Fix default submit button issues on some forms

[1.5.1]
* Update ctfreak to 1.14.2
* [Full changelog](https://ctfreak.com/docs/changelog#v1-14-2)
* Fix gitlab incoming webhook calls
* Improve logs of failed incoming webhook calls

[1.6.0]
* Update ctfreak to 1.15.0
* [Full changelog](https://ctfreak.com/docs/changelog#v1-15-0)
* Add optional HTTP response logging (status, headers and body)
* JSON response body auto-prettifying
* Add FreeBSD amd64 & arm64 build and install process
* Add Linux arm64 build
* Remove Linux arm32 build
* Back-end is now built with Go 1.22
* Update dependencies
* Update internal log timestamp format

[1.6.1]
* Update ctfreak to 1.16.0
* [Full changelog](https://ctfreak.com/docs/changelog#v1-16-0)
* CTFreak can now be used as a read-only SFTP web client for all your nodes
* Smoother UI navigation with improved concurrent read-only access to backend database
* Update dependencies
* Fix saving of node filter tag exclusion when editing tasks

[1.7.0]
* Update ctfreak to 1.17.0
* [Full changelog](https://ctfreak.com/docs/changelog#v1-17-0)
* Execution retention period can now be specified at the project or task level
* Files can be deleted in Node file explorer (requires at least STARTUP Edition)
* Add a delay between failed login attempts to prevent brute-force attacks
* Execution lists can be dynamically filtered by task name
* Node lists can be dynamically filtered by node name

[1.7.1]
* Update ctfreak to 1.18.0
* [Full changelog](https://ctfreak.com/docs/changelog#v1-18-0)
* Maximum number of concurrent child task executions is now configurable for workflow tasks that execute child tasks concurrently
* Child tasks can be disabled/re-enabled (involves a breaking change in child task structure when reading/creating/updating workflow tasks through the API)
* Drag & drop replaces move buttons to reorder Workflow Child tasks
* Update dependencies
* Remove unwanted child tasks deduplication displayed in Workflow task pages

[1.7.2]
* Update ctfreak to 1.18.1
* [Full changelog](https://ctfreak.com/docs/changelog#v1-18-1)
* Fix regression preventing bash script, powershell script, and sql report tasks creation

[1.8.0]
* Update ctfreak to 1.19.0
* [Full changelog](https://ctfreak.com/docs/changelog#v1-19-0)

[1.9.0]
* Update ctfreak to 1.20.0
* [Full changelog](https://ctfreak.com/docs/changelog#v1-20-0)

[1.10.0]
* Update ctfreak to 1.21.0
* [Full changelog](https://ctfreak.com/docs/changelog#v1-21-0)
* Local command tasks support per-second scheduling granularity via an optional parameter for their cron expression (e.g. 20 * * * * * for execution every minute at the 20th second)
* When a local command execution is aborted under linux & freebsd, this abort also applies to child processes
* Execution retention period can now be set to 0 days (for tasks where only the last execution status/date needs to be retained)
* By default, the internal database stores its temporary data in memory. This can now be disabled in the configuration file via the new useMemoryTempStoreFg flag

[1.11.0]
* Update ctfreak to 1.22.0
* [Full changelog](https://ctfreak.com/docs/changelog#v1-22-0)
* Integrated API documentation available at: `https://{YOUR CTFREAK INSTANCE}/api/v1/doc`
* Execution timeout can now be specified at the global or project level
* Global execution timeout is mandatory (30 days default)
* At the very least, the global execution timeout is used, so an execution always has a defined timeout.
* Improve logs
* Update dependencies

[1.11.1]
* Update ctfreak to 1.22.1
* [Full changelog](https://ctfreak.com/docs/changelog#v1-22-1)
* Use relative URL instead of external URL defined in global settings to access integrated API documentation

[1.11.2]
* checklist added to CloudronManifest
* COUDRON_OIDC_PROVIDER_NAME implemented

[1.12.0]
* Update ctfreak to 1.23.0
* [Full Changelog](https://ctfreak.com/docs/changelog#v1-23-0)
* Redirect to the page initially requested (rather than the home page) after logging in again via the internal authentication provider
* SPA build: replace Rollup with Vite
* Update dependencies

[1.12.1]
* Update ctfreak to 1.23.1
* [Full Changelog](https://ctfreak.com/docs/changelog#v1-23-1)

