#!/bin/bash

set -eu

echo "=> Ensure permissions"
chown -R cloudron:cloudron /app/data

echo "=> Starting ctfreak server"
if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    exec gosu cloudron:cloudron /app/code/ctfreak -c /app/data run -set-external-url="${CLOUDRON_APP_ORIGIN}" -set-main-oidc-auth-provider-name="${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}" -set-main-oidc-auth-provider-client-id="${CLOUDRON_OIDC_CLIENT_ID}" -set-main-oidc-auth-provider-client-secret="${CLOUDRON_OIDC_CLIENT_SECRET}" -set-main-oidc-auth-provider-discovery-endpoint="${CLOUDRON_OIDC_DISCOVERY_URL}"
else
    exec gosu cloudron:cloudron /app/code/ctfreak -c /app/data run -set-external-url="${CLOUDRON_APP_ORIGIN}"
fi
