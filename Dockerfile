FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=custom.ctfreak depName=ctfreak versioning=semver
ARG CTFREAK_VERSION=1.23.1

RUN curl -L https://ctfreak.com/download/release/${CTFREAK_VERSION}/linux/amd64 -o /app/code/ctfreak \
    && chmod a+x /app/code/ctfreak

COPY start.sh /app/code/

CMD [ "/app/code/start.sh" ]

